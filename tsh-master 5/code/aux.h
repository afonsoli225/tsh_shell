#include <unistd.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

char *strremove(char *str, const char *sub);
int   nbrofLink(char* tar,char *dir);
char* gettoken(char* arg);
char* gettoken1(char* arg);
int cat_file(int fd, struct posix_header *header, int taille){
	char *name = malloc(strlen(header->name)+1);
	assert(name);
	strcpy(name, header->name);
    char *contenubrut = malloc(taille*BLOCKSIZE);
	assert(contenubrut);
    read(fd, contenubrut, taille*BLOCKSIZE);
	char *contenu = malloc(strlen(contenubrut)+1);
	assert(contenu);
	strncpy(contenu, contenubrut, strlen(contenubrut));
	write(STDOUT_FILENO, contenu, strlen(contenu));
	close(fd);
    return 1;
}
int write_tar(int fdtar, struct posix_header* header, char* name){
	int fdfic = open(name, O_RDONLY);
	if((write(fdtar, header, BLOCKSIZE)) < BLOCKSIZE) return -1;
	int taille = 0;
	sscanf(header->size, "%o", &taille);
	int filesize = ((taille + 512-1)/512);
	char *content = malloc(taille);
	assert(content);
	for (int i = filesize*BLOCKSIZE - taille; i < filesize*BLOCKSIZE; i++) {
		content[i] = '\0';
	}
    if (write(fdtar, content, filesize*BLOCKSIZE) < filesize*BLOCKSIZE) return -1;
	close(fdfic);
	return 1;
}
int write_end(int fd){
	char *endblock = malloc(BLOCKSIZE*2);
	assert(endblock);
	for(int i = 0; i < BLOCKSIZE*2; i++){
		endblock[i] = '\0';
	}
	if((write(fd, endblock, BLOCKSIZE*2)) < BLOCKSIZE*2) return -1;
	else return 0;
}
void ls_l(char *arg){
	    int position=0;
        for(int i=0;i<strlen(arg);i++){
            if(arg[i]=='.'&&arg[i+1]=='t'&&arg[i+2]=='a'&&arg[i+3]=='r'&&arg[i+4]=='/'){
                position=i+4;
            }
        }
        if(position==0){
            return;
        }
        char tar[position];
        char dir[strlen(arg)-position-1];
        for(int i=0;i<strlen(arg);i++){
            if(i<position){
                tar[i]=arg[i];
            }else if(i>position){
                dir[i-position-1]=arg[i];
            }
        }
        tar[position]='\0';
        dir[strlen(arg)-position-1]='\0';
        if(dir[strlen(dir)-1]!='/'){
            strcat(dir,"/");
        }
        struct posix_header * header = malloc(sizeof(struct posix_header));
        assert(header);
        int fd = open(tar, O_RDONLY);
        if(fd == -1){
          perror("erreur d'ouverture du fichier");
          return ;
        }
        char* name;
        int n = 0;
        int countlink=2;
        while((n=read(fd, header, BLOCKSIZE))>0){
            if(strcmp(header->name, "\0") == 0){
                return ;
            }
            int taille = 0;
            int *ptaille = &taille;
            sscanf(header->size, "%o", ptaille);
            int time=0;
            int *ptime=&time;
            sscanf(header->mtime, "%o", ptime);
            time_t x = time;
            char *realtime=ctime(&x);
            assert(realtime);
            char *ptr = header->mode;
            int filesize = ((*ptaille + 512-1)/512);
            int i;
            realtime[strlen(realtime)-1] = '\0';
            if(strstr(header->name,dir)!=NULL){
                const char s[2] = "/";
                char *token;
                name=header->name;
                name=strremove(name,dir);
                token = strtok(name, s);
                token = strtok(NULL, s);
                if(name[0]!='\0'&&token==NULL&&name[0]!='.'){   
                    if(header->typeflag=='5'){
                        
                        write(STDOUT_FILENO, "d", 1);
                    }else{
                        write(STDOUT_FILENO, "-", 1);
                    }
                    for(i=3;i<=5;i++){
                        if(ptr[i]=='7'){
                            write(STDOUT_FILENO, "rwx", 3);
                        }else if(ptr[i]=='6'){
                            write(STDOUT_FILENO, "rw-", 3);
                        }else if(ptr[i]=='5'){
                            write(STDOUT_FILENO, "r-x", 3);
                        }else if(ptr[i]=='4'){
                            write(STDOUT_FILENO, "r--", 3);
                        }else if(ptr[i]=='3'){
                            write(STDOUT_FILENO, "-wx", 3);
                        }else if(ptr[i]=='2'){
                            write(STDOUT_FILENO, "-w-", 3);
                        }else if(ptr[i]=='1'){
                            write(STDOUT_FILENO, "--x", 3);
                        }else if(ptr[i]=='0'){
                            write(STDOUT_FILENO, "---", 3);
                        }
                    }
                    if(header->typeflag=='5'){
                        countlink=countlink+nbrofLink(tar,header->name);
                        char c[50];
                        sprintf(c," %d", countlink);
                        write(STDOUT_FILENO, c, strlen(c));
                        
                    }else{
                        write(STDOUT_FILENO, " 1",2);
                    }
                    write(STDOUT_FILENO, " ", 1);
                    write(STDOUT_FILENO, header->uname, strlen(header->uname));
                    
                    write(STDOUT_FILENO, " ", 1);
                    write(STDOUT_FILENO, header->gname, strlen(header->gname));
      
                    char t[50];
                    if(taille >=10000){
                        sprintf(t,"%d", taille);
                        write(STDOUT_FILENO, t, strlen(t));
                       
                    }else if(taille >=1000){
                        sprintf(t," %d ", taille);
                        write(STDOUT_FILENO, t, strlen(t));
                
                    }else if(taille >=100){
                        sprintf(t," %d ", taille);
                        write(STDOUT_FILENO, t, strlen(t));
                        
                    }else if(taille >=10){
                        printf(" %4d ",taille);
                    } else{
                        printf(" %4d ",taille);
                    }
                    write(STDOUT_FILENO, " ", 1);
                    write(STDOUT_FILENO, realtime, strlen(realtime));
                    write(STDOUT_FILENO, " ", 1);
                    write(STDOUT_FILENO, name, strlen(name));
                    write(STDOUT_FILENO,"\n",2);
                }
            }
            read(fd, header, BLOCKSIZE*filesize);
        }
    write(STDOUT_FILENO,"\n",2);
    close(fd);
    return;
}
void ls(char *arg){
	int position;
        for(int i=0;i<strlen(arg);i++){
            if(arg[i]=='.'&&arg[i+1]=='t'&&arg[i+2]=='a'&&arg[i+3]=='r'&&arg[i+4]=='/'){
                position=i+4;
            }
        }
        
        char *tar = (char *) malloc(sizeof(char *) * position);
        char dir[strlen(arg)-position-1];
        for(int i=0;i<strlen(arg);i++){
            if(i<position){
                tar[i]=arg[i];
            }else if(i>position){
                dir[i-position-1]=arg[i];
            }
        }
        tar[position]='\0';
        dir[strlen(arg)-position-1]='\0';
        if(dir[strlen(dir)-1]!='/'){
            strcat(dir,"/");
        }
        struct posix_header * header = malloc(sizeof(struct posix_header)*512);
        assert(header);
        int fd = open(tar, O_RDONLY);
        if(fd == -1){
          perror("erreur d'ouverture du fichier");
          return ;
        }
        int n = 0;
      
        while((n=read(fd, header, BLOCKSIZE))>0){
            if(strcmp(header->name, "") == 0){
                close(fd);
                return ;
            }

            int taille = 0;
            int *ptaille = &taille;
            sscanf(header->size, "%o", ptaille);
            
            int filesize = ((*ptaille + 512-1)/512);
        
            if(strstr(header->name,dir)!=NULL){
                const char s[2] = "/";
                char *token;
                char *nametmp = (char *) malloc(sizeof(char *));
                assert(nametmp);
                char* name = (char *) malloc(sizeof(char*));
                assert(name);
                int itt = 1;
                strcpy(name, header->name);
                token = strtok(dir, s);
                while (token != NULL){
                    token = strtok(NULL,s);
                    itt++;
                }
                token = strtok(name,s);
                while( 0 < itt){
                    if(token == NULL){
                        name[0] = '\0';
                    }else{
                        strcpy(nametmp, token);
                        nametmp[strlen(nametmp)] = '/';
                    }
                    token = strtok(NULL,s);
                    itt--;
                }
                if(name[0]!='\0'&&token==NULL&&name[0]!='.'){   
                    write(STDOUT_FILENO,nametmp,strlen(nametmp));
                    write(STDOUT_FILENO,"\n",2);
                }
            }

            if (filesize > 0){
                if(read(fd, header, BLOCKSIZE*filesize) <= 0){
                }
            }
        }
    write(STDOUT_FILENO,"\n",2);
    close(fd);
    return;
}

int nbrofLink(char* tar,char *dir){


	struct posix_header * header = malloc(sizeof(struct posix_header));
	assert(header);
	int fd = open(tar, O_RDONLY);
	if(fd == -1){
	    perror("erreur d'ouverture du fichier");
		return 0;
	}
	int n = 0;
	int countlink=0;
	
	while((n=read(fd, header, BLOCKSIZE))>0){

			if(strcmp(header->name, "\0") == 0){
				
				return countlink-2;
			}
			int taille = 0;
			int *ptaille = &taille;
			sscanf(header->size, "%o", ptaille);
			int filesize = ((*ptaille + 512-1)/512);
			if(strstr(header->name,dir)!=NULL){
				countlink++;
			}
			read(fd, header, BLOCKSIZE*filesize);
		}
		close(fd);
		return countlink;
}
char *strremove(char *str, const char *sub) {
    char *p, *q, *r;
    if ((q = r = strstr(str, sub)) != NULL) {
        size_t len = strlen(sub);
        while ((r = strstr(p = r + len, sub)) != NULL) {
            while (p < r)
                *q++ = *p++;
        }
        while ((*q++ = *p++) != '\0')
            continue;
    }
    return str;
}
int rm_aux(char *arg1, char *arg2){
        struct posix_header * header = malloc(sizeof(struct posix_header));
        assert(header);
        int found = 0;
        int fdtar = open(arg1, O_RDWR , S_IRWXU);
        while(!found){
            if(read(fdtar,header,BLOCKSIZE)<BLOCKSIZE) break;
            char nom[strlen(header->name)+1];
            strcpy(nom,header->name);
            int taille=0;
            int *ptaille=&taille;
            sscanf(header->size,"%o",ptaille);
            int filesize=((*ptaille+512-1)/512);
            if(strcmp(nom,arg2)==0){
                off_t position;
                off_t end_position;
                if((position=lseek(fdtar,-BLOCKSIZE,SEEK_CUR))==-1) break;
                if((end_position=lseek(fdtar,0,SEEK_END))==-1) break;
                unsigned int size=end_position -(position+BLOCKSIZE*filesize);
                char cpy[size];
                if(lseek(fdtar,position+BLOCKSIZE+BLOCKSIZE*filesize,SEEK_SET)==-1) break;
                read(fdtar,cpy,size);
                if(lseek(fdtar,position,SEEK_SET)==-1) break;
                if(write(fdtar,cpy,size)<size){
                    write(STDOUT_FILENO,"erreur",6);
                    write(STDOUT_FILENO,"\n",2);
                }
                found=1;
                break;
            }
            read(fdtar,header,BLOCKSIZE*filesize);
        }

        close(fdtar);
    
    return 1;
}
int rm_r_aux(char *arg1, char *arg2){
    struct posix_header * header = malloc(sizeof(struct posix_header));
    assert(header);
    int found = 0;
    int found1= 0;
    off_t position;
    off_t end_position;
    int fdtar = open(arg1, O_RDWR , S_IRWXU);
    while(!found1){
        if(read(fdtar,header,BLOCKSIZE)<BLOCKSIZE) {
            found1=1;
        }
        char nom[strlen(header->name)+1];
        strcpy(nom,header->name);
        int taille=0;
        int *ptaille=&taille;
        sscanf(header->size,"%o",ptaille);
        int filesize=((*ptaille+512-1)/512);
        if(strstr(nom,arg2)!=NULL){
            if(found==0&&found1==0){
                if((position=lseek(fdtar,-BLOCKSIZE,SEEK_CUR))==-1) break;
                found =1;
                lseek(fdtar,BLOCKSIZE,SEEK_CUR);
                if((end_position=lseek(fdtar,BLOCKSIZE*filesize,SEEK_CUR))==-1) break;
                lseek(fdtar,-BLOCKSIZE*filesize,SEEK_CUR);
            }else{
                if((end_position=lseek(fdtar,BLOCKSIZE*filesize,SEEK_CUR))==-1) break;
                lseek(fdtar,-BLOCKSIZE*filesize,SEEK_CUR);
            }
        }
        read(fdtar,header,BLOCKSIZE*filesize);
    }
    unsigned int size=lseek(fdtar,0,SEEK_END)-end_position;
    char cpy[size];
    if(lseek(fdtar,end_position,SEEK_SET)==-1){
        write(STDOUT_FILENO,"erreur",6);
        write(STDOUT_FILENO,"\n",2);
    }
    read(fdtar,cpy,size);
    if(lseek(fdtar,position,SEEK_SET)==-1){
        write(STDOUT_FILENO,"erreur",6);
        write(STDOUT_FILENO,"\n",2);
    }
    write(fdtar,cpy,size);
    close(fdtar);
    return 1;
}
char* gettoken(char* arg){
    int pos=0;
    for(int i=0;i<strlen(arg);i++){
        if(arg[i]=='.'&&arg[i+1]=='t'&&arg[i+2]=='a'&&arg[i+3]=='r'&& arg[i+4]=='/'){
            pos=i+4;
            }
        }
        char *token= malloc(pos);
        char token1[strlen(arg)-pos];
        for (int i=0;i<pos;i++){
            token[i]=arg[i];
        }
        token[pos]='\0';
        for (int i=pos;i<strlen(arg);i++){
            token1[i-pos]=arg[i+1];
        }
        return token;
}
char* gettoken1(char* arg){
    int pos=0;
    for(int i=0;i<strlen(arg);i++){
        if(arg[i]=='.'&&arg[i+1]=='t'&&arg[i+2]=='a'&&arg[i+3]=='r'&& arg[i+4]=='/'){
            pos=i+4;
            }
        }
        char token[pos];
        char *token1=malloc(strlen(arg)-pos);
        for (int i=0;i<pos;i++){
            token[i]=arg[i];
        }
        token[pos]='\0';
        for (int i=pos;i<strlen(arg);i++){
            token1[i-pos]=arg[i+1];
        }
        return token1;
}
int goingToTar(char* arg){
    //int pos=0;
    for(int i=0;i<strlen(arg);i++){
        if(arg[i]=='.'&&arg[i+1]=='t'&&arg[i+2]=='a'&&arg[i+3]=='r'&& arg[i+4]=='/'){
            return 1;
            }
    }
    return 0;
}