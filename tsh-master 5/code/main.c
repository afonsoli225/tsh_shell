//
// Created by hspinat on 21/10/2020.
//
#include "tar.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <dirent.h>
#include <time.h>
#include <pwd.h>
#include <grp.h>
#include <sys/stat.h>
#include <assert.h>
#include <errno.h>
#include "aux.h"

#define TSH_RL_BUFSIZE 1024
#define TSH_TOK_BUFSIZE 64
#define TSH_TOK_DELIM " \t\r\n\a"
#define BUFFER_SIZE 1024

/*
    Declarations des fonctions pour les commandes
 */
int tsh_cd(char **args);//fait
int tsh_help(char **args);//fait
int tsh_exit(char **args);//fait
int tsh_pwd(char **args);//julien
int tsh_mkdir(char **args);//julien
int tsh_rmdir(char **args);//julien
int tsh_mv(char **args);// a faire
int tsh_cp(char **args);// a faire
int tsh_rm(char **args);//fait
int tsh_ls(char **args);//fait
int tsh_cat(char **args);//fait
int tsh_pipe(char ** args);// a faire

char tsh_pwdtar[1000]; //utilise dans chdir
char current_tar[1000];


/*
    Liste des commandes, suivies de leurs fonctions correspondantes
 */
char *builtin_str[] = {
        "cd",
        "help",
        "exit",
        "pwd",
        "mkdir",
        "rmdir",
        "mv",
        "cp",
        "rm",
        "ls",
        "cat"
        
};

int (*builtin_func[]) (char **) = {
        &tsh_cd,
        &tsh_help,
        &tsh_exit,
        &tsh_pwd,
        &tsh_mkdir,
        &tsh_rmdir,
        &tsh_mv,
        &tsh_cp,
        &tsh_rm,
        &tsh_ls,
        &tsh_cat
};


//Taille du tableau des commandes
int tsh_num_builtins() {
    return sizeof(builtin_str) / sizeof(char *);
}



/*
    Implementations des fonctions
*/
char *tsh_getcwd(char *buf, size_t size){
    char *rep = getcwd(buf,size);
    if(strcmp(current_tar,"") == 0){
        return rep;
    }
    char *cwdtmp = (char*) malloc(sizeof(char*)*1000);
    strcpy(cwdtmp,getcwd(buf,size));
    rep = strcat(strcat(cwdtmp,"/(.tar)"),tsh_pwdtar);
    return rep;
}


int tsh_chdir(char *path){
    const char *d = "/";
    char *tmp = strtok(path,d);
    char *pathtmp = (char*) malloc(sizeof(char)*1000);
    strcpy(pathtmp,tsh_pwdtar);
    
    while(tmp != NULL){
        if(strcmp(current_tar,"") != 0){ //Si on est dans un tar 
            int ftar = open(current_tar,O_RDONLY);
            if(ftar == -1) {//Le fichier ne peut pas etre ouvert
                char *error = "errno: ";
                char *er=NULL;
                sprintf(er,"%d",errno);
                strcat(error,er);
                strcat(error,"\n");
                if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
                    perror("Erreur d'écriture dans le shell!");
                return -1;
            }
            if(strcmp(tmp,".") == 0){ //Si c'est cd ./
                tmp = strtok(NULL,d);
            }else if(strcmp(tmp,("..")) == 0){ //Si c'est cd ../
                long unsigned int pos = 0;
                long unsigned int pos2 = 0;
                for(long unsigned int i=0; i<strlen(pathtmp);i++){
                    if(pathtmp[i] == '/'){
                        pos2 = pos;
                        pos = i;
                    }
                }
                if(pos < (strlen(pathtmp)-1)){ //Si on est sortit du tar
                    strcpy(current_tar,"");
                    strcpy(tsh_pwdtar,"");
                    strcpy(pathtmp,"");
                }else{
                    if (pos2 == 0){
                        strcpy(pathtmp, "");
                        strcpy(tsh_pwdtar, "");
                    }else{
                        char cutpath[pos2+1];
                        memcpy(cutpath,pathtmp,pos2);
                        cutpath[pos2] = '\0';
                        strcat(cutpath,"/");
                        strcpy(pathtmp,cutpath);
                    }
                    
                
                }
                
                tmp = strtok(NULL,d);
            }else{ 
                while(tmp != NULL && (strcmp(tmp,".") != 0) && strcmp(tmp,"..") != 0 ){
                    strcat(strcat(pathtmp,tmp),"/");
                    tmp = strtok(NULL,d);
                }
            }
            close(ftar);
        }//Si on est pas dans un tar
        else{
            if (chdir(tmp) != 0){
                if(strstr(tmp,".tar") != NULL){
                    int ftar = open(tmp,O_RDONLY);
                    if(ftar == -1) {
                        char *error = "errno:";
                        char *er=NULL;
                        sprintf(er,"%d",errno);
                        strcat(error,er);
                        strcat(error,"\n");
                        if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
                            perror("Erreur d'écriture dans le shell!");
                        return -1;
                    }
                    strcpy(current_tar,tmp);
                    /*char cuttmp[strlen(tmp)-3];
                    memcpy(cuttmp,tmp,strlen(tmp)-4);
                    cuttmp[strlen(tmp)-4] = '\0';
                    strcat(strcat(pathtmp,cuttmp),"/");*/
                    tmp = strtok(NULL,d);
                    close(ftar);
                }
                else{//le fichier tmp n'existe pas
                    perror("tsh");
                    return -1;
                }
            }else{
                tmp = strtok(NULL,d);
            }
        }
    }//On sort du while
    if(strcmp(pathtmp,"")!=0){ //On cherche si le chemin existe et on se deplace
        int found = 0;
        int r,l;
        struct posix_header *ph = malloc(sizeof(struct posix_header));
        int ftar = open(current_tar,O_RDONLY);
        if(ftar == -1) {
            char *error = "errno:";
            char *er=NULL;
            sprintf(er,"%d",errno);
            strcat(error,er);
            strcat(error,"\n");
            if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
                perror("Erreur d'écriture dans le shell!");
            free(ph);
            free(pathtmp);
            close(ftar);
            return -1;
        }
        while(found == 0){
            struct posix_header *buffer = malloc(sizeof(struct posix_header));
            r = read(ftar,buffer,BLOCKSIZE);
            l = lseek(ftar,0L,SEEK_CUR);
            if( r < BLOCKSIZE) found = -1;
            memcpy(ph->name, buffer, 100);
            if(strcmp(ph->name, pathtmp) == 0) {
                found = 1;
            }
        }
        free(ph);
        if(found == -1){ //erreur dossier introuvable
            char *error = "Impossible d'acceder a ce chemin.:";
            strcat(error,"\n");
            if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
                    perror("Erreur d'écriture dans le shell!");
            if(strcmp(tsh_pwdtar,"") == 0){
                strcpy(current_tar,"");
            }
            close(ftar);
            return -1;
        }
        if(found == 1){ //dossier trouvé
            strcpy(tsh_pwdtar,pathtmp);
            close(ftar);
            free(pathtmp);
            return 0;
        }
        close(ftar);
    }
    free(pathtmp);
    return 0;
}


//cd
int tsh_cd(char **args)
{
    if (args[1] == NULL) {
        fprintf(stderr, "tsh: des arguments sont attendus après \"cd\"\n");
    } else {
        tsh_chdir(args[1]);
    }
    return 1;
}


//help
int tsh_help(char **args)
{
    int i;
    write(STDOUT_FILENO,"Hugo, Afonso and Julien's tsh\n",31);
    write(STDOUT_FILENO,"Entrez le nom d'une commande et tapez entrer.\n",47);
    write(STDOUT_FILENO,"Voici nos commandes:\n",22);
    for (i = 0; i < tsh_num_builtins(); i++) {
        write(STDOUT_FILENO,builtin_str[i],strlen(builtin_str[i]));
        write(STDOUT_FILENO,"\n",2);
    }
    return 1;
}


//exit
int tsh_exit(char **args)
{
    return 0;
}


//commande pwd
int tsh_pwd(char **args){
    char *pwd = tsh_getcwd(NULL,0);
    char *print = (char*) malloc(sizeof(char)*(strlen(pwd)+1));
    strcpy(print,pwd);
    strcat(print,"\n");

    if(write(STDERR_FILENO, print, strlen(print)) < strlen(print))
            perror("Erreur d'écriture dans le shell!");
    return 1;
}

//Utilisé dans tsh_mkdir pour creer un dossier impliqué dans un tarball
int mktar(char *tardir, char *tarpath){ 
    int found = 0;
    const char *d = "/";
    char *tarpathrep = (char*) malloc(sizeof(char)*strlen(tarpath));
    memcpy(tarpathrep,tarpath,strlen(tarpath));
    char *tmp = strtok(tarpath,d);
    char *tmptmp = (char*) malloc(sizeof(char)*1000);
    char *tarpathtmp = (char*) malloc(sizeof(char)*1000);
    struct posix_header *ph = malloc(sizeof(struct posix_header));
    struct posix_header *rep = malloc(sizeof(struct posix_header));

    //Le dossier que l'on veut creer
    memcpy(rep->name, tarpathrep, strlen(tarpathrep)); //set name
    sprintf(rep->mode, "00700"); //set mode
    sprintf(rep->size, "%0o", 4096); //set size
    rep->typeflag = '5'; //set flag
    memcpy(rep->magic, TMAGIC, strlen(TMAGIC)); //set magic
    set_checksum(rep); //set checksum 
    
    int ftar = open(tardir,O_RDWR | O_CREAT, S_IRWXU);
    
    strcpy(tarpathtmp,"");
    while(tmp != NULL){
        
        strcpy(tmptmp,tmp);
        if((tmp = strtok(NULL,d)) == NULL){//On est arrivé à la fin du chemin
            //On cherche tarpath dans ftar à l'aide de ph
            if(strcmp(tarpathtmp,"") == 0){
                //memcpy(rep->name, tarpathrep, strlen(tarpathrep)); //set name

                if(write(ftar,rep,BLOCKSIZE) < BLOCKSIZE){
                    perror("Erreur d'écriture dans le tarball");
                }

                int taille = 0;
                int *ptaille = &taille;
                sscanf(rep -> size, "%o", ptaille);
                int filesize = ((*ptaille + 512 - 1) / 512);
                char *content = malloc(taille);
                assert(content);

                for (int i = filesize*BLOCKSIZE - taille; i < filesize*BLOCKSIZE; i++) {
                    content[i] = '\0';
                }

                lseek(ftar, 0L, SEEK_END);
                if (write(ftar, content, filesize*BLOCKSIZE) < filesize*BLOCKSIZE){
                    perror("Erreur d'écriture dans le tarball");
                } 

                free(content);
                free(ph);
                close(ftar);
                free(tmptmp);
                free(tarpathtmp);
                free(tarpathrep);
                free(rep);
                return 1;
            }else{
                while(!found){
                    //chercher tarpathtmp
                    int n = 0;
                    if((n = read(ftar, ph, BLOCKSIZE)) < BLOCKSIZE){
                        found = -1;
                    } 

                    char name[strlen(ph -> name) +1];
                    strcpy(name, ph -> name);

                    if(strcmp(name, tarpathtmp) == 0){
                        found = 1;
                    }
                }
            }
            
        }else{
            strcat(tarpathtmp,tmptmp);
            strcat(tarpathtmp,"/");
        }
    }

    if(found == -1){
        char *error = "Le fichier ou dossier n'existe pas";
        if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
            perror("Erreur d'écriture dans le shell!");
    }else if(found == 1){
        int wri = 0;
        //écrire rep a la suite de ftar
        lseek(ftar,0L,SEEK_SET);
        while(read(ftar, ph, BLOCKSIZE) == BLOCKSIZE ){
            if( strcmp(ph->name,"") == 0 && wri == 0){
                lseek(ftar, -BLOCKSIZE, SEEK_CUR);

                //Mon write se passe mal
                if(write(ftar, rep, BLOCKSIZE) < BLOCKSIZE){
                    char *error = "errno:";
                    char *er=NULL;
                    sprintf(er,"%d",errno);
                    strcat(error,er);
                    strcat(error,"\n");
                    if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
                        perror("Erreur d'écriture dans le shell!");
                }
                wri = 1;
                lseek(ftar, -BLOCKSIZE, SEEK_CUR);
                read(ftar,ph,BLOCKSIZE);
            }
        }
    }
    free(ph);
    close(ftar);
    free(tmptmp);
    free(tarpathtmp);
    free(tarpathrep);
    free(rep);
    return 1;
}

char *PreDir(char *path){//Utilisé dans tsh_mkdir qui désigne le dossier père
    int i = 0;
    int tmp = 0;
    int pos;
    while(path[i] != '\0'){
        if(path[i] == '/'){
            pos = tmp;
            tmp = i;
        }
        i++;
    }
    char rep[pos+1];
    memcpy(rep,path,pos);
    rep[pos] = '\0';
    if(strcmp(rep,"") != 0){
        strcat(rep,"/");
    }
    char *reptmp = rep;
    return reptmp;
}

//isInTar(path) renvoie le tableau [dos,tarpath] où dos est le dossier où se trouve path et tarpath est le chemin du path dans le tar si il est dans un tarball
char **isInTar(char *path){
    char **rep = (char **) malloc(sizeof(char*)*2);
    if(path == NULL){
        char *error = "mkdir: opérande manquant \nSaisissez un nom en argument.\n";
        if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
            perror("Erreur d'écriture dans le shell!");
    }
    const char *d = "/";
    char *tmp = strtok(path,d);
    char *tarpath = (char*) malloc(sizeof(char)*1000);
    char *tardir = (char*) malloc(sizeof(char)*1000);
    char *tmptmp = (char*) malloc(sizeof(char)*1000);
    strcpy(tardir, current_tar);
    strcpy(tarpath, tsh_pwdtar);
    while(tmp != NULL){
        //On se déplace jusqu'à l'avant dernier dossier du chemin
        strcpy(tmptmp, tmp);
        if((tmp = strtok(NULL,d)) != NULL){ //Si on était pas au dernier dossier
            if(strcmp(tmptmp,"..") == 0){
                if(strcmp(tarpath,"") != 0){ //On est dans le tar et on va vers le dossier père
                    char *pere = PreDir(tarpath); //Le nom du dossier père
                    strcpy(tarpath,pere);
                    if(strcmp(tarpath,"") == 0){//On est sortit du tar
                        strcpy(tardir,"");
                    }
                }else{//On n'est pas dans le tar
                    strcat(tardir,"..");
                    strcat(tardir,"/");
                }
            }else if( strcmp(tmptmp,".") == 0){
                //on fait rien
            }else{//On veut aller à tmptmp
            
                if(strcmp(tarpath,"") != 0){ //On est encore dans le tar
                    if(strstr(tmptmp,".tar") != NULL){ //On veut se déplacer dans un .tar
                        char *error = "mkdir: .tar imbriqué impossible";
                        if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
                            perror("Erreur d'écriture dans le shell!");
                    }else{
                        char *tardirtmp = (char*) malloc(sizeof(char)*(strlen(tardir)-1));
                        strncpy(tardirtmp,tardir,strlen(tardir));
                        int op = open(tardirtmp,O_RDONLY);
                        if(op == -1){
                            char *error = "mkdir: Aucun fichier ou dossier de ce type";
                            if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
                                perror("Erreur d'écriture dans le shell!");

                        }else{
                            close(op);
                            strcat(tarpath,tmptmp);
                            strcat(tarpath,"/");
                        }
                        free(tardirtmp);
                    }
                }else{//On n'est pas dans le tar
                    strcat(tardir,tmptmp);
                    if(strstr(tmptmp,".tar") != NULL){
                        char tarpathcut[strlen(tmptmp)-3];
                        memcpy(tarpathcut,tmptmp,strlen(tmptmp)-4);
                        tarpathcut[strlen(tmptmp)-4] = '\0';

                        strcat(tarpathcut,"/");
                        free(tarpath);
                        tarpath = (char*) malloc(sizeof(char)*strlen(tarpathcut));
                        memcpy(tarpath,tarpathcut,strlen(tarpathcut));
                        tarpath[strlen(tarpathcut)] = '\0';
                    }
                }
            }
        }else{//Si on est au dernier dossier
            if(strcmp(tarpath,"") == 0){//On est sortit du tar
                if(strstr(tmptmp,".tar") != NULL){ //Si on veut creer un .tar
                    if(strstr(tardir,".tar") != NULL){ //Si on s'est déplacé dans un tar
                        char *error = "mkdir: .tar imbriqué impossible";
                        if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
                            perror("Erreur d'écriture dans le shell!");
                    }else{
                        strcat(strcat(tardir,tmptmp),"/");

                        char tarpathcut[strlen(tmptmp)-3];
                        memcpy(tarpathcut,tmptmp,strlen(tmptmp)-4);
                        tarpathcut[strlen(tmptmp)-4] = '\0';

                        strcpy(tarpath,tarpathcut);
                        strcat(tarpath,"/");
                        char *tardirtmp = (char*) malloc(sizeof(char)*(strlen(tardir)-1));
                        strncpy(tardirtmp,tardir,strlen(tardir)-1);
                       

                        rep[0] = (char*) malloc(sizeof(char)*(strlen(tardirtmp)+1));
                        strncpy(rep[0],tardir,strlen(tardir));
                        rep[0][strlen(tardir)] = '\0';

                        rep[1] = (char*) malloc(sizeof(char)*(strlen(tarpath)+1));
                        strncpy(rep[1],tarpath,strlen(tarpath));
                        rep[1][strlen(tarpath)] = '\0';

                        free(tardirtmp);
                        free(tardir);
                    }
                }else{//aucun rapport avec tarball
                    strcat(tardir,"/");
                    strcat(tardir,tmptmp);
                    rep[0] = (char*) malloc(sizeof(char)*(strlen(tardir)+1));
                    strncpy(rep[0],tardir,strlen(tardir));
                    rep[0][strlen(tardir)] = '\0';
                    rep[1] = (char*) malloc(sizeof(char)*(strlen(tarpath)+1));
                    strncpy(rep[1],tarpath,strlen(tarpath));
                    rep[1][strlen(tarpath)] = '\0';

                    free(tardir);
                }
            }else{//On est resté dans un .tar
                strcat(strcat(tarpath,tmptmp),"/");
                if(strstr(tmptmp,".tar") != NULL){
                    char *error = "mkdir: .tar imbriqué impossible";
                    if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
                        perror("Erreur d'écriture dans le shell!");
                }else{
                    
                    rep[0] = (char*) malloc(sizeof(char)*(strlen(tardir)+1));
                    strncpy(rep[0],tardir,strlen(tardir));
                    rep[0][strlen(tardir)] = '\0';

                    rep[1] = (char*) malloc(sizeof(char)*(strlen(tarpath)+1));
                    strncpy(rep[1],tarpath,strlen(tarpath));
                    rep[1][strlen(tarpath)] = '\0';
                }
            }
        }
    }
    free(tarpath);
    free(tmptmp);
    return rep;
}


//Commande mkdir pour tarball
int tsh_mkdir(char **args){
    int i = 1,rep = 1;
    if(args[1] == NULL){
        char *error = "mkdir: opérande manquant \nSaisissez un nom en argument.\n";
        if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
            perror("Erreur d'écriture dans le shell!");
        return 1;
    }
    while(args[i] != NULL){
        char **tmp = (char**) malloc(sizeof(char*));
        tmp = isInTar(args[i]);

        if(strstr(tmp[0],".tar") != NULL){ //le dossier args[i] est dans un .tar
            if(strstr(tmp[1],".tar") != NULL){//.tar imbriqué
                char *error = ".tar imbriqué impossible";
                if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
                    perror("Erreur d'écriture dans le shell!");
            }else{
                mktar(tmp[0],tmp[1]);
            }
        }else{//le dossier arg[i] n'est pas dans un .tar
            if(strstr(tmp[1],".tar") != NULL){//On veut creer un .tar
                mktar(tmp[0],tmp[1]);
            }else{//mkdir normal
                char *error = "C'est un mkdir normal";
                return -1;
                if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
                    perror("Erreur d'écriture dans le shell!");
            }
        }
        i++;
    }
    return rep;
}



//commande rmdir 
int tsh_rmdir(char **argv){
    if(argv[1]==NULL) write(STDOUT_FILENO,"que voulez vous supprimer?",26);
    else{
        int a=1;
        if(strlen(tsh_pwdtar)==0||strlen(current_tar)==0){
            while(argv[a]!=NULL){
                int pos=0;
                for(int i=0;i<strlen(argv[1]);i++){
                    if(argv[a][i]=='.'&&argv[a][i+1]=='t'&&argv[a][i+2]=='a'&&argv[a][i+3]=='r'&& argv[a][i+4]=='/'){
                        pos=i+4;
                    }
                }
                char token[pos];
                char token1[strlen(argv[a])-pos];
                for (int i=0;i<pos;i++){
                    token[i]=argv[a][i];
                }
                token[pos]='\0';
                for (int i=pos;i<strlen(argv[a]);i++){
                    token1[i-pos]=argv[a][i+1];
                }
                char *file = token;
                struct posix_header * header = malloc(sizeof(struct posix_header));
                assert(header);
                int found = 0;
                int fdtar = open(file, O_RDWR + O_CREAT, S_IRWXU);
                while(!found){
                if(read(fdtar,header,BLOCKSIZE)<BLOCKSIZE) break;
                char nom[strlen(header->name)+1];
                strcpy(nom,header->name);
                int taille=0;
                int *ptaille=&taille;
                sscanf(header->size,"%o",ptaille);
                int filesize=((*ptaille+512-1)/512);
                if(strcmp(nom,token1)==0){
                    off_t position;
                    off_t end_position;
                    if((position=lseek(fdtar,-BLOCKSIZE,SEEK_CUR))==-1) break;
                    if((end_position=lseek(fdtar,0,SEEK_END))==-1) break;
                    unsigned int size=end_position -(position+BLOCKSIZE);
                    char cpy[size];
                    if(lseek(fdtar,position+BLOCKSIZE,SEEK_SET)==-1) break;
                    read(fdtar,cpy,size);
                    if(lseek(fdtar,position,SEEK_SET)==-1) break;
                    write(fdtar,cpy,size);
                    found=1;
                    break;
                }
                read(fdtar,header,BLOCKSIZE*filesize);
            }
            close(fdtar);
            a++;
        }
        return 1;
        }else{
            while(argv[a]!=NULL){
                char begin[strlen(argv[a])+strlen(tsh_pwdtar)];
                strcpy(begin,tsh_pwdtar);
                strcat(begin,argv[a]);
                struct posix_header * header = malloc(sizeof(struct posix_header));
                assert(header);
                int found = 0;
                int fdtar = open(current_tar, O_RDWR + O_CREAT, S_IRWXU);
                while(!found){
                if(read(fdtar,header,BLOCKSIZE)<BLOCKSIZE) break;
                char nom[strlen(header->name)+1];
                strcpy(nom,header->name);
                int taille=0;
                int *ptaille=&taille;
                sscanf(header->size,"%o",ptaille);
                int filesize=((*ptaille+512-1)/512);
                if(strcmp(nom,begin)==0){
                    off_t position;
                    off_t end_position;
                    if((position=lseek(fdtar,-BLOCKSIZE,SEEK_CUR))==-1) break;
                    if((end_position=lseek(fdtar,0,SEEK_END))==-1) break;
                    unsigned int size=end_position -(position+BLOCKSIZE);
                    char cpy[size];
                    if(lseek(fdtar,position+BLOCKSIZE,SEEK_SET)==-1) break;
                    read(fdtar,cpy,size);
                    if(lseek(fdtar,position,SEEK_SET)==-1) break;
                    write(fdtar,cpy,size);
                    found=1;
                    break;
                }
                read(fdtar,header,BLOCKSIZE*filesize);
            }
            close(fdtar);
            a++;
            }
            return 1;
        }
    }
    return 1;
}


//commande mv
int tsh_mv(char **args){
    
    return 1;
}



int copyFromAtoB(char* tardirA,char *tarpathA,char* tardirB,char *tarpathB){//return 2 si on essaye de copier un repertoir
    int ftarA = open(tardirA,O_RDONLY);
    int ftarB = open(tardirB,O_RDWR | O_CREAT, S_IRWXU);
    if(ftarA == -1){
        char *error_debut = "cp: impossible d'évaluer ";
        char *error_fin = " ; Aucun fichier ou dossier de ce type\n";
        char error[strlen(error_debut) + strlen(tardirA) + strlen(error_fin)];
        strcpy(error,error_debut);
        strcat(error,tardirA);
        strcat(error,error_fin);
        if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
            perror("Erreur d'écriture dans le shell!");
        return -1;
    }
    struct posix_header *headerA = malloc(sizeof(struct posix_header));
    struct posix_header *headerB = malloc(sizeof(struct posix_header));
    char cpy[512];
    unsigned int size;
    int found = 0;
    if(strcmp(tarpathA,"") == 0){//Si on veut copier un fichier qui n'a rien a voir avec les tar
        if(read(ftarA,headerA,BLOCKSIZE) < 0){
            found = -1;
            char *error_debut = "errno:";
            char er[3];
            char *error_fin = "\n";
            int i = errno;
            sprintf(er,"%d",i);
            char error[strlen(error_debut) + 3 + 2];
            strcat(error,er);
            strcat(error,error_fin);
            if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
                perror("Erreur d'écriture dans le shell!");
        }
        
        if(headerA -> typeflag != 0){ //Si c'est pas un fichier normal
            return 2;
        }
        size = lseek(ftarA, 0L,SEEK_END);
        char cpy[size];
        lseek(ftarA,0L,SEEK_SET);
        read(ftarA,cpy,size);
    }else{
        while(!found){
            if(read(ftarA,headerA,BLOCKSIZE) <= 0){
                found = -1;
                char *error_debut = "errno:";
                char er[3];
                char *error_fin = "\n";
                int i = errno;
                sprintf(er,"%d",i);
                char error[strlen(error_debut) + 3 + 2];
                strcat(error,er);
                strcat(error,error_fin);
                if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
                    perror("Erreur d'écriture dans le shell!");
            }
            if(strcmp(headerA -> name, tarpathA) == 0) {
                found = 1;

                int taille=0;
                sscanf(headerA->size, "%o", &taille);
                int filesize = ((taille + 512-1)/512);
                char *content = malloc(taille);
                assert(content);

                //lseek(ftarA,BLOCKSIZE*filesize,SEEK_CUR);

				unsigned int size = BLOCKSIZE*filesize;

				//char cpy[size];
				read(ftarA,cpy,size);
            }
        }
    }
    if(found == 1){
        if(headerA -> typeflag != '0'){ //Si A n'est pas un fichier normal
            return 2;
        }
        lseek(ftarA,0L,SEEK_SET);
    }else if(found == -1){
        char *error = "cp: Aucun fichier ou dossier de ce type\n";
        if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
            perror("Erreur d'écriture dans le shell!");
    }


    //On cherche B
    found = 0;
    if(strcmp(tarpathB,"") == 0){//Si on veut coller vers un fichier qui n'a rien a voir avec les tar
        lseek(ftarB,0L,SEEK_SET);
        write(ftarB,cpy,strlen(cpy));
        while(read(ftarB,headerB,BLOCKSIZE) > 0){ //On cherche un emplacement vide
            if(strcmp(headerB -> name, "") == 0){
                int taille=0;
                sscanf(headerB->size, "%o", &taille);
                int filesize = ((taille + 512-1)/512);
                //char *content = malloc(taille);

                if(lseek(ftarB,-BLOCKSIZE*filesize,SEEK_CUR)==-1) break;
                write(ftarB,cpy,strlen(cpy));
            }
        }
    }else{
        while(!found){ //On cherche si le fichier B existe deja

            if(read(ftarB,headerB,BLOCKSIZE) <= 0){
                found = -1;
            }

            if(strcmp(headerB -> name, tarpathA) == 0) { //Si le fichier existe deja
                found = 1;

                if(headerB -> typeflag == '5'){//Si on veut ecrire dans un repertoir
                    int i = 0,j,k;
                    while(tardirA[i]){ //On cherche à couper tardirA pour n'avoir que le nom du fichier conscerné
                        if(tardirA[i] == '/'){
                            k = j;
                            j = i;
                        }
                        i++;
                    }
                    char ficnameA[j-k];
                    memcpy(ficnameA,&tardirA[k], j-k);
                    char *ficnameB = (char*) malloc(sizeof(char));
                    memcpy(ficnameB,headerB -> name,strlen(headerB->name));
                    strcat(ficnameB,ficnameA);
                    
                    memcpy(headerB -> name, ficnameB,strlen(ficnameB));
                }

			    int taille=0;
                sscanf(headerB->size, "%o", &taille);
                int filesize = ((taille + 512-1)/512);
                char *content = malloc(taille);
                assert(content);
                
                off_t position;
				off_t endfile;

                if((position=lseek(ftarB,-BLOCKSIZE*filesize,SEEK_CUR))==-1) break;
				endfile=lseek(ftarB,0,SEEK_END);

				unsigned int sizend = endfile -(position+BLOCKSIZE*filesize);

				char cpyend[sizend];
				if(lseek(ftarB,position+BLOCKSIZE*filesize,SEEK_SET)==-1) break;
				read(ftarB,cpyend,sizend);

				if(lseek(ftarB,position,SEEK_SET)==-1) break;
				write(ftarB,cpyend,sizend);

                write(ftarB,cpy,strlen(cpy));
            }
        }
        if(found == -1){ //Si le nom du fichier n'existe pas
            lseek(ftarB,0L,SEEK_SET);
            int wri = 0;
            while(read(ftarB,headerB,BLOCKSIZE) > 0 && wri == 0){ //On cherche un emplacement vide

                if(strcmp(headerB -> name, "") == 0){


                    struct posix_header *rep = malloc(sizeof(struct posix_header));

                    //Le dossier que l'on veut creer
                    memcpy(rep->name, tarpathB, strlen(tarpathB)); //set name

                    sprintf(rep->mode, "00700"); //set mode
                    sprintf(rep->size, "%0o", 4096); //set size
                    rep->typeflag = '0'; //set flag
                    memcpy(rep->magic, TMAGIC, strlen(TMAGIC)); //set magic
                    set_checksum(rep); //set checksum 

                    if(lseek(ftarB,-BLOCKSIZE,SEEK_CUR)==-1) break;
                    write(ftarB,rep,BLOCKSIZE);
                    write(ftarB,cpy,strlen(cpy));
                    wri = 1;
                }
            }
        }
    }
    return 1;
}

//commande cp
int tsh_cp(char **args){

    
	if(strcmp(args[2],"") == 0){
        char *error = "cp: opérande de fichier cible manquant après ";
        strcat(error,args[1]);
        strcat(error,"\n");
        if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
            perror("Erreur d'écriture dans le shell!");
        return 1;
    }

    if(strcmp (args[1], "-r") != 0){//cp sans -r
        if(args[3] == NULL){ //cp avec moins de 3 arguments
            char *tarpathA = (char*) malloc(sizeof(char)*strlen(args[0]));
            char *tarpathB = (char*) malloc(sizeof(char)*strlen(args[1]));
            strcpy(tarpathA, args[1]);
            strcpy(tarpathB, args[2]);

            char **tabA = (char**) malloc(sizeof(char*));
            char **tabB = (char**) malloc(sizeof(char*));

            tabA = isInTar(tarpathA); //IsinTar retourne un tableau [dir,path] avec dir est le dossier ou le fichier se trouve et path est le chemin du fichier dans un tarball
            tabB = isInTar(tarpathB);

            if(tabA[0][strlen(tabA[0])-1] == '/'){
                strncpy(tabA[0],tabA[0],strlen(tabA[0])-1);
                tabA[0][strlen(tabA[0])-1] = '\0';
            }else if(tabA[0][0] == '/'){
                strncpy(tabA[0],&tabA[0][1],strlen(tabA[0])-1);
                tabA[0][strlen(tabA[0])-1] = '\0';
            }

            if(tabA[1][strlen(tabA[1])-1] == '/'){
                strncpy(tabA[1],tabA[1],strlen(tabA[1])-1);
                tabA[1][strlen(tabA[1])-1] = '\0';
            }
            
            if(tabB[0][strlen(tabB[0])-1] == '/'){
                strncpy(tabB[0],tabB[0],strlen(tabB[0])-1);
                tabB[0][strlen(tabB[0])-1] = '\0';
            }else if(tabB[0][0] == '/'){
                strncpy(tabB[0],&tabB[0][1],strlen(tabB[0])-1);
                tabB[0][strlen(tabB[0])-1] = '\0';
            }

            if(tabB[1][strlen(tabB[1])-1] == '/'){
                strncpy(tabB[1],tabB[1],strlen(tabB[1])-1);
                tabB[1][strlen(tabB[1])-1] = '\0';
            }


            if(copyFromAtoB(tabA[0],tabA[1],tabB[0],tabB[1]) == 2){
                char *error = "cp: -r non spécifié \n";
                if(write(STDERR_FILENO, error, strlen(error)) < strlen(error))
                    perror("Erreur d'écriture dans le shell!");

            } //Si return 2 -> cp -r

        }
        
    }
    return 1;
}




//commande rm
int tsh_rm(char **argv){
    if(strcmp(argv[1],"-r")==0){//rm -r
        if(strlen(tsh_pwdtar)==0||strlen(current_tar)==0){
            if(argv[2]==NULL){
               return -1;
           }else{
                int tmp=2;
                while(argv[tmp]!=NULL){
                    int pos=0;
                    for(int i=0;i<strlen(argv[tmp]);i++){
                        if(argv[tmp][i]=='.'&&argv[tmp][i+1]=='t'&&argv[tmp][i+2]=='a'&&argv[tmp][i+3]=='r'&& argv[tmp][i+4]=='/'){
                            pos=i+4;
                        }
                    }
                    char token[pos];
                    char token1[strlen(argv[tmp])-pos];
                    for (int i=0;i<pos;i++){
                        token[i]=argv[tmp][i];
                    }
                    token[pos]='\0';
                    for (int i=pos;i<strlen(argv[tmp]);i++){
                        token1[i-pos]=argv[tmp][i+1];
                    }
                    if(rm_r_aux(token,token1)!=1){
                        return-1;
                    }
                    tmp++;
                }
                
            }
        }else{
            int tmp=2;

            while(argv[tmp]!=NULL){
                if(goingToTar(argv[tmp])==0){
                    char begin[strlen(argv[tmp])+strlen(tsh_pwdtar)];
                    strcpy(begin,tsh_pwdtar);
                    strcat(begin,argv[tmp]);
                    if(rm_r_aux(current_tar,begin)!=1){
                        return -1;
                    }

                }else{
                   char *token=gettoken(argv[tmp]);
                   tsh_chdir(token);
                   char token1[strlen(gettoken1(argv[tmp]))];
                   for (int i = 0; i < strlen(gettoken1(argv[tmp])); ++i)
                   {
                       token1[i]=gettoken1(argv[tmp])[i];
                   }
                   token1[strlen(gettoken1(argv[tmp]))]='\0';
                   if(rm_r_aux(current_tar,token1)!=1){
                        return -1;
                    }
                }
                tmp++;
            }
            return 1;
        }
    }else{//rm simple 
        if(strlen(tsh_pwdtar)==0||strlen(current_tar)==0){
            if(argv[1]==NULL){
                return -1;
           }else{
                int tmp=1;
                while(argv[tmp]!=NULL){
                    int pos=0;
                    for(int i=0;i<strlen(argv[tmp]);i++){
                        if(argv[tmp][i]=='.'&&argv[tmp][i+1]=='t'&&argv[tmp][i+2]=='a'&&argv[tmp][i+3]=='r'&& argv[tmp][i+4]=='/'){
                            pos=i+4;
                        }
                    }
                    char token[pos];
                    char token1[strlen(argv[tmp])-pos];
                    for (int i=0;i<pos;i++){
                        token[i]=argv[tmp][i];
                    }
                    token[pos]='\0';
                    for (int i=pos;i<strlen(argv[tmp]);i++){
                        token1[i-pos]=argv[tmp][i+1];
                    }
                    if(rm_aux(token,token1)!=1){
                        return-1;
                    }
                    tmp++;
                }
                
           }
        }else{
            int tmp=1;

            while(argv[tmp]!=NULL){
                if(goingToTar(argv[tmp])==0){
                    char begin[strlen(argv[tmp])+strlen(tsh_pwdtar)];
                    strcpy(begin,tsh_pwdtar);
                    strcat(begin,argv[tmp]);
                    if(rm_r_aux(current_tar,begin)!=1){
                        return -1;
                    }

                }else{
                   char *token=gettoken(argv[tmp]);
                   tsh_chdir(token);
                   char token1[strlen(gettoken1(argv[tmp]))];
                   for (int i = 0; i < strlen(gettoken1(argv[tmp])); ++i)
                   {
                       token1[i]=gettoken1(argv[tmp])[i];
                   }
                   token1[strlen(gettoken1(argv[tmp]))]='\0';
                   if(rm_r_aux(current_tar,token1)!=1){
                        return -1;
                    }
                }
                tmp++;
            }
            return 1;
        }
    }
    return 1;
}
//commande ls
int tsh_ls(char **argv){
    if(argv[1]==NULL && strlen(current_tar)==0){
            return -1;
    }
    else if(argv[1] != NULL && strcmp(argv[1],"-l")==0){//ls -l 
        if(strlen(current_tar)==0){
            if(argv[2]!=NULL){
               ls_l(argv[2]); 
           }else{
                return -1;
           }
        }else{
            int taille=0;
            
            if(argv[2]==NULL){
                taille=strlen(current_tar)+strlen(tsh_pwdtar);
            }else{
                taille=strlen(current_tar)+strlen(tsh_pwdtar)+strlen(argv[2]);
            }
            char begin[taille];
            
            for(int i=0;i<taille;i++){
                if(i<strlen(current_tar)){
                    begin[i]=current_tar[i];
                }else if(i==strlen(current_tar)){
                    begin[i]='/';
                }else {
                    begin[i]=tsh_pwdtar[i-1-strlen(current_tar)];
                }
            }
            if(argv[2]!=NULL){
                strcat(begin,argv[2]);
            }
            ls_l(begin);
        }
    }else{//ls simple 
        if(strlen(current_tar)==0){
            if(argv[1]!=NULL){
                char *begin = (char *) malloc(sizeof(char*)*strlen(argv[1]));
                strcpy(begin,argv[1]);
                begin[strlen(begin)] = '/';
                ls(begin); 
           }else{
                return -1;
           }
        }else{
            int taille=0;
           
            if(argv[1]==NULL){
                taille=strlen(current_tar)+strlen(tsh_pwdtar);
            }else{
                taille=strlen(current_tar)+strlen(tsh_pwdtar)+strlen(argv[1]);
            }
            char *begin = (char *) malloc(sizeof(char*)*taille);
            strcpy(begin,current_tar);
            strcat(begin, "/");
            if (strcmp(tsh_pwdtar, "") != 0){
                strcat(begin, tsh_pwdtar);
            }
            begin[strlen(begin)-1] = '/';
            if(argv[1]!=NULL){
                if(strcmp(argv[1], "..") != 0){
                    strcat(begin,argv[1]);
                }
            }
            printf("begin: %s\n",begin);
            ls(begin);
        free(begin);
        }
    }
    return 1;
}



//commande cat
int tsh_cat(char **argv){
    if(argv[0]==NULL||argv[1]==NULL) write(STDOUT_FILENO,"Aucun fichier passé en paramètre !\n",36);
    else if(strlen(tsh_pwdtar)==0||strlen(current_tar)==0){
        struct posix_header * header = malloc(sizeof(struct posix_header));
        assert(header);
        int fd;
        int a=1;
        int tmp=0;
        while(argv[tmp]!=NULL){
            tmp++;
        }
        while(a!=tmp){
            int pos=0;
            for(int i=0;i<strlen(argv[1]);i++){
                if(argv[a][i]=='.'&&argv[a][i+1]=='t'&&argv[a][i+2]=='a'&&argv[a][i+3]=='r'&& argv[a][i+4]=='/'){
                    pos=i+4;
                }
            }
            char token[pos];
            char token1[strlen(argv[a])-pos];
            for (int i=0;i<pos;i++){
                token[i]=argv[a][i];
            }
            token[pos]='\0';
            for (int i=pos;i<strlen(argv[a]);i++){
                token1[i-pos]=argv[a][i+1];
            }
            fd = open(token, O_RDONLY);
            if(fd == -1){
              perror("Je laisse mon ami exec faire le travail");
              return -1;
            }
            while(read(fd, header, BLOCKSIZE)>0){
                int taille = 0;
                int *ptaille = &taille;
                sscanf(header->size, "%o", ptaille);
                int filesize = ((*ptaille + 512-1)/512);
                
                if(strcmp((header->name), token1) == 0&&header->typeflag=='0'){
                    cat_file(fd, header, filesize);

                }else if(strcmp((header->name), token1) == 0&&header->typeflag=='5'){
                }
                read(fd, header, BLOCKSIZE*filesize);
            }
            close(fd);
            a++;
        }
        close(fd);
    }else{//dans un tar
       
        struct posix_header * header = malloc(sizeof(struct posix_header));
        assert(header);
        int fd;
        int a=1;
        int tmp=0;
        while(argv[tmp]!=NULL){
            tmp++;
        }
        while(a!=tmp){
            char begin[strlen(argv[a])+strlen(tsh_pwdtar)];
            strcpy(begin,tsh_pwdtar);
            strcat(begin,argv[a]);
            fd = open(current_tar, O_RDONLY);
            if(fd == -1){
              perror("erreur d'ouverture du fichier");
              return -1;
            }
            while(read(fd, header, BLOCKSIZE)>0){
                int taille = 0;
                int *ptaille = &taille;
                sscanf(header->size, "%o", ptaille);
                int filesize = ((*ptaille + 512-1)/512);
                
                if(strcmp((header->name), begin) == 0&&header->typeflag=='0'){
                    
                    cat_file(fd, header, filesize);

                }else if(strcmp((header->name), begin) == 0&&header->typeflag=='5'){
                }
                read(fd, header, BLOCKSIZE*filesize);
            }
            close(fd);
            a++;
        }
        close(fd);
    }
    return 1;
}


int  tsh_pipe(char ** args){
    // a faire
    return 1;
}



int tsh_launch(char **args){
    pid_t pid, wpid;
    int status;
    pid = fork();
    if (pid == 0) {
        // processus fils
        if (execvp(args[0], args) == -1) {
            
            perror("tsh");
        }
        exit(EXIT_FAILURE);
    } else if (pid < 0) {
        // Erreur
        perror("tsh");
    } else {
        // processus pere
        do {
            wpid = waitpid(pid, &status, WUNTRACED);
        } while (!WIFEXITED(status) && !WIFSIGNALED(status));
    }

    return 1;
}


//fonction qui execute les fonctions commandes
int tsh_execute(char **args){
    int i;
    if (args[0] == NULL ) {
        // aucune commande
        return 1;
    }
    for (i = 0; i < tsh_num_builtins(); i++) {
        if(args[1]!=NULL){
            if (strcmp(args[0], builtin_str[i]) == 0 ) {
                if((*builtin_func[i])(args)==1){
                    return 1;
                }else{
                    return tsh_launch(args);
                }
            }else if(strcmp(args[1], "|") == 0){
                //printf("%s\n","pipe" );
                if(tsh_pipe(args)==1){
                    return 1;
                }else{
                    return tsh_launch(args);
                }
            }
        }else{
            if (strcmp(args[0], builtin_str[i]) == 0) {
                if((*builtin_func[i])(args)==1){
                    return 1;
                }else if(strcmp(args[0],"exit")==0){
                    return 0;
                }else{
                    return tsh_launch(args);
                }
            }
        }
    }
    return tsh_launch(args);
}


//fonction qui separe un string en plusieurs string utilisant les delimiteurs
char **tsh_split_line(char *line){
    int bufsize = TSH_TOK_BUFSIZE, position = 0;
    char **tokens = malloc(bufsize * sizeof(char*));
    char *token;

    if (!tokens) {
        fprintf(stderr, "tsh: allocation error\n");
        exit(EXIT_FAILURE);
    }

    token = strtok(line, TSH_TOK_DELIM);
    while (token != NULL) {
        tokens[position] = token;
        position++;

        if (position >= bufsize) {
            bufsize += TSH_TOK_BUFSIZE;
            tokens = realloc(tokens, bufsize * sizeof(char*));
            if (!tokens) {
                fprintf(stderr, "tsh: allocation error\n");
                free(tokens);
                exit(EXIT_FAILURE);
            }
        }

        token = strtok(NULL, TSH_TOK_DELIM);
    }
    tokens[position] = NULL;
    return tokens;
}


//fonction qui recupere ce qui est dans stdin qui retourne un string ou char*
char *tsh_read_line(void)
{
    int bufsize = TSH_RL_BUFSIZE;
    int position = 0;
    char *buffer = malloc(sizeof(char) * bufsize);
    int c;

    if (!buffer) {
        fprintf(stderr, "tsh: erreur d'allocation\n");
        exit(EXIT_FAILURE);
    }

    while (1) {
        // lit un caractere
        c = getchar();

        //Si on tape un EOF alors on le remplace avec le caracter null 
        if (c == EOF || c == '\n') {
            buffer[position] = '\0';
            return buffer;
        } else {
            buffer[position] = c;
        }
        position++;

        // Si ca depasse le buffer, on fait un realloc
        if (position >= bufsize) {
            bufsize += TSH_RL_BUFSIZE;
            buffer = realloc(buffer, bufsize);
            if (!buffer) {
                fprintf(stderr, "tsh: erreur d'allocation\n");
                free(buffer);
                exit(EXIT_FAILURE);
            }
        }
    }
    free(buffer);
}


//commande qui fait une loop, lit la ligne tapé, interprète le tout et exécute la commande.
void tsh_loop(void){
    char *line;
    char **args;
    int status;

    do {
        printf("\n");
        tsh_pwd(NULL);
        write(STDOUT_FILENO,">>> ",4);
        write(STDOUT_FILENO,"tsh_pwdtar: ",12);
        write(STDOUT_FILENO,tsh_pwdtar,strlen(tsh_pwdtar));
        write(STDOUT_FILENO,", current_tar: ",14);
        write(STDOUT_FILENO, current_tar,strlen(current_tar));
        write(STDOUT_FILENO,"\n",2);
        line = tsh_read_line();
        args = tsh_split_line(line);
        status = tsh_execute(args);

        free(line);
        free(args);
    } while (status);
}


//main
int main(int argc, char **argv){
    tsh_loop();
    
    return 0;
}
