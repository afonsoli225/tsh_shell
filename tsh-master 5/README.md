Pour lancer notre shell, veuillez suivre les instructions suivantes:


Sur votre terminal, entrez make puis ./main.
Vous voici maintenant dans notre shell.
Pour voir les commandes disponibles, entrez help dans la ligne de commandes.

À vous de tester les commandes.

Attention: Certaines commandes ne sont pas encore complètes telles que: cp -r, mv et les |.

Si vous ne réussissez pas à lancer notre programme. Voici une vidéo qui peut vous aider https://youtu.be/TR68zublkUQ

Pour l'utilisation du Dockerfile taper:
docker build -t nom . 
docker run -ti nom    