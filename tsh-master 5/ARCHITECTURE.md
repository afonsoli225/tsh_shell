Tout commence par la fonction tsh_loop() qui fait une boucle pour lire 
les commandes écrites dans le main. 
Nous avons un tableau builtin_str[] où est écrit toutes les commandes acceptables
par notre Shell. Si les commandes sont reconnues, tshloop() lance tsh_execute()
qui prend la commande et les arguments pour les traiter. Si les fonctions ne sont
pas compatibles avec nos fonctions, on lance tsh_launch() qui sert a fork() pour
lancer un exec().

Notre code commence d'abord avec tous les include notemment celui de "tar.h"
fournit au premier TP. Ensuite nous avons la déclaration de nos fonctions et
deux variables tsh_pwdtar(qui désigne le chemin du répertoire courrant dans un tar)
et current_tar(qui désigne le fichier .tar où nous sommes si on est dans un .tar).
Nous avons ensuite la liste des commandes prisent en compte par notre Shell et le tableau
qui liste les fonctions que nous utilisons suivant la commande reçue.
Puis vous voilà dans la partie "Implémentation des fonctions" qui sont toutse les
fonctions que nous avons écrites (les principales et les auxilliiaires).
Enfin, il y a le main qui vient en dernier.

Les algorithmes implémentés sont propre au style de chacun, par exemple Julien
a créé la fontion isInDir() qui l'aide à traiter les informations pour bien les
utiliser dans ses fontcions.
Afonso quant à lui a écrit ses fonctions en dehors du main.c et les a implémenté
ensuite à l'intèrieur.

Malheureusement, nous n'avons pas eu le temps d'écrire toutes les fonctions et 
il peut y avoir quelques bugs non fixés.

Nous nous sommes rendu compte que notre premier rendu était un peu hors sujet,
mais nous avon reussit (nous espérons du moins) avoir répondu aux attentes du 
sujet.